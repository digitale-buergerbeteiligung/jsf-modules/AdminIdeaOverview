package servicetrackers;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import com.liferay.mail.kernel.service.MailService;

public class MailLocalServiceTracker extends ServiceTracker<MailService, MailService> {

    public MailLocalServiceTracker(BundleContext bundleContext){
        super(bundleContext, MailService.class, null);
    }
}
