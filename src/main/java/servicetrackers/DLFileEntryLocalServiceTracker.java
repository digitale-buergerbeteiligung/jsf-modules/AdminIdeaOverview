package servicetrackers;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import com.liferay.document.library.kernel.service.DLFileEntryLocalService;

public class DLFileEntryLocalServiceTracker extends ServiceTracker<DLFileEntryLocalService, DLFileEntryLocalService> {
    public DLFileEntryLocalServiceTracker(BundleContext bundleContext){
        super(bundleContext, DLFileEntryLocalService.class, null);
    }
}
