package servicetrackers;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import analysisService.service.SessionTimeLocalService;

public class SessionTimeLocalServiceTracker extends ServiceTracker<SessionTimeLocalService, SessionTimeLocalService>{
	public SessionTimeLocalServiceTracker(BundleContext bundleContext){
        super(bundleContext, SessionTimeLocalService.class, null);
    }
}
