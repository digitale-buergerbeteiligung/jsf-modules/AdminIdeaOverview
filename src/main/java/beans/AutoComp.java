package beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import org.primefaces.event.SelectEvent;

@ManagedBean(name="autoCompBean")
public class AutoComp {


	@ManagedProperty("#{tagService}")
    private TagService service;

	public List<String> completeText(String query){
		List<String> results = new ArrayList<String>();
		List<String> allTags = service.getTags();

		//removes white spaces from the start and end
		query = query.trim();

        for(int i = 0; i < allTags.size(); i++){
        	if(allTags.get(i).toLowerCase().startsWith(query.toLowerCase())){
            	results.add(allTags.get(i));
            }
        }

        if(!service.getTempTags().contains(query.toLowerCase())){
        	results.add(query.toLowerCase());
        }
        return results;
    }

	public void onItemSelect(SelectEvent event){
		String selectedItem = (String)event.getObject();
		if(!service.getTags().contains(selectedItem)){
			service.setTags(selectedItem.toLowerCase());
		}
		service.setTempTags(selectedItem.toLowerCase());
    }

	public void setService(TagService service){
        this.service = service;
    }

    public TagService getService(){
    	return this.service;
    }

}